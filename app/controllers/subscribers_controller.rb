class SubscribersController < ApplicationController
  respond_to :xml

  def index
    @subscribers = Subscribers.all
    render :xml => @subscribers

    #respond_to do |format|
    #  format.html # index.html.erb
    #  format.json { render json: @subscribers }
    #
    # end
  end

  def show
    @subscribers = Subscribers.find(params[:id])
    render :xml => @subscribers

    #respond_to do |format|
    #  format.html # show.html.erb
    #  format.json { render json: @subscribers }
    #end
  end

end
