class CreateSubscribers < ActiveRecord::Migration
  def change
    create_table :subscribers do |t|
      t.string :Name
      t.integer :Phone
      t.integer :ID_Number

      t.timestamps
    end
  end
end
