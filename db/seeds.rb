# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

Subscribers.create([{Name:"Anna",Phone:1234567890,ID_Number:1}])
Subscribers.create([{Name:"Alexander",Phone:9876543211,ID_Number:2}])
Subscribers.create([{Name:"Fred",Phone:9195551212,ID_Number:3}])
Subscribers.create([{Name:"Gerald",Phone:9090000000,ID_Number:4}])